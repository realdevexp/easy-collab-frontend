import React, { useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import { func, shape } from 'prop-types';
import { Row, Col, Typography, Tabs } from 'antd';
import '../css/form.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import '../css/global.css';
import { Helmet } from 'react-helmet';
import { QueryClientProvider, QueryClient } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import Login from './Authentication/Login';
import Register from './Authentication/Register';
import OAuth from './Authentication/OAuth';
import GithubAuth from './Authentication/GithubAuth';
import Dashboard from './Authentication/Dashboard';
import Error from './Authentication/Error';
import ForgotPassword from './Authentication/ForgotPassword';
import ResetPassword from './Authentication/ResetPassword';

const { TabPane } = Tabs;
// import StoreDemo from './StoreDemo';

const queryClient = new QueryClient();

const App = ({ t, i18n }) => {
  useEffect(() => {
    i18n.changeLanguage('en');
  }, [i18n]);
  const { Text, Title } = Typography;

  return (
    <Router>
      <QueryClientProvider client={queryClient}>
        <Switch>
          <Route path="/oauth">
            <OAuth />
          </Route>

          <Route path="/projects">
            <h2 className="">Projects</h2>
          </Route>

          <Route path="/Dashboard">
            <Dashboard />
          </Route>

          <Route path="/Error">
            <Error />
          </Route>

          <Route path="/forgot_password">
            <ForgotPassword />
          </Route>

          <Route path="/reset_password">
            <ResetPassword />
          </Route>

          <Route
            path="/:route"
            render={({ match, history }) => {
              if (
                match.params.route === 'login' ||
                match.params.route === 'register'
              ) {
                return (
                  <>
                    <div className="form--wrapper">
                      <Row>
                        <Col span={24} offset={0}>
                          <Title className="site--logo">
                            {t('easy_collab')}
                          </Title>
                          <div className="form--container">
                            <Title level={3}>
                              {t('tasks.login_to_easycollab')}
                            </Title>
                            <Text type="secondary">
                              {t('tasks.continue_with_email')}
                            </Text>
                            <br />
                            <br />
                            <Switch>
                              <>
                                <Tabs
                                  activeKey={match.params.route}
                                  onChange={(route) => {
                                    history.push(`/${route}`);
                                  }}
                                >
                                  <TabPane tab="Login" key="login">
                                    <Helmet>
                                      <title>
                                        {t('tasks.login')} •{' '}
                                        {t('easy_collab')}
                                      </title>
                                    </Helmet>
                                    <Login />
                                  </TabPane>
                                  <TabPane
                                    tab="Register"
                                    key="register"
                                  >
                                    <Helmet>
                                      <title>
                                        {t('tasks.register')} •{' '}
                                        {t('easy_collab')}
                                      </title>
                                    </Helmet>
                                    <Register />
                                  </TabPane>
                                </Tabs>
                              </>
                            </Switch>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  </>
                );
              }
              return null;
            }}
          />
          <Route path="/login/github" exact>
            <GithubAuth />
          </Route>

          <Route path="/" exact>
            <Helmet>
              <title />
              {t('home')} • {t('easy_collab')}
            </Helmet>
            {t('errors.unauthorized_access')}
          </Route>
        </Switch>
        <ReactQueryDevtools />
      </QueryClientProvider>
    </Router>
  );
};

App.propTypes = {
  t: func.isRequired,
  i18n: shape({ changeLanguage: func }).isRequired,
};

export default withTranslation()(App);
