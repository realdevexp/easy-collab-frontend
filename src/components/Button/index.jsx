import React, { useEffect, useState } from 'react';
import ButtonProps, { LoadingTextProps } from './props';
import './Button.css';
// Button only component

const LoadingText = ({ text }) => (
  <div className="flex">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="h-6 w-6 animate-spin"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15"
      />
    </svg>
    <p className="ml-2 mb-0 flex items-center">{text}</p>
  </div>
);
LoadingText.propTypes = LoadingTextProps.propTypes;
LoadingText.defaultProps = LoadingTextProps.defaultProps;

/**
 * @type {HTMLButtonElement}
 * @param {string} variant - Button Variant [small | large]
 * @param {string} type - HTML button type - [submit | button | reset ]
 * @param {node} text - i18n Text Key
 * @param {array} other - Other Attributes
 * @param {string} color - [blue | green]
 * @param {boolean} loading adds button loading stylings if set to true.
 * @param {string} loadingText - Text To be inserted If 'loading' is true.
 * @param {boolean} disabled - Adds disabled styling.
 * @param {array} HTMLButtonElementAttributes - Other HTMLButtonElement Attributes
 * @returns {node} Button
 *
 */
const Button = ({
  variant,
  type,
  color,
  children,
  loading,
  loadingText,
  disabled,
  ...HTMLButtonElementAttributes
}) => {
  const [btnVariant, setBtnVariant] = useState('');
  useEffect(() => {
    // if user mistypes
    const formattedVariant = variant.trim().toLowerCase();

    if (formattedVariant === 'small') {
      setBtnVariant('btn-small');
    } else if (formattedVariant === 'large') {
      setBtnVariant('btn-large');
    } else {
      console.warn('Invalid variant provided to button field.');
    }
  }, [variant]);
  return (
    <button
      className={`btn btn-${color} ${btnVariant} ${
        loading && 'btn-loading'
      } ${disabled && 'btn-faded'}`}
      type={type || 'button'}
      disabled={loading || disabled}
      {...HTMLButtonElementAttributes}
    >
      {loading ? <LoadingText text={loadingText} /> : children}
    </button>
  );
};

Button.propTypes = ButtonProps.propTypes;
Button.defaultProps = ButtonProps.defaultProps;

export default Button;
