/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Divider, Alert } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { withTranslation } from 'react-i18next';
import { useMutation } from 'react-query';
import { func } from 'prop-types';
import { forgotPassword } from '../../api/auth';

function ForgotPassword({ t }) {
  const [form] = Form.useForm();
  const [isUserRegistered, setisUserRegistered] = useState(false);

  const { mutate, isError, error, isSuccess, isLoading } =
    useMutation(forgotPassword);

  useEffect(() => {
    if (isError && error && error.response.status === 401) {
      setisUserRegistered(true);
      form.validateFields();
    }
  }, [isError, error, form]);

  const onFinish = (values) => {
    mutate(values);
  };

  return (
    <>
      {!isSuccess ? (
        <Form
          form={form}
          name="normal_login"
          className="form--container login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="email"
            data-testid="email"
            rules={[
              {
                required: true,
                message: t('form_errors.email_required'),
              },
              {
                pattern: new RegExp(
                  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                ),
                message: t('form_errors.valid_email_id'),
              },
              {
                validator: () =>
                  !isUserRegistered
                    ? Promise.resolve()
                    : Promise.reject(
                        new Error(t('errors.email_in_use')),
                      ),
              },
            ]}
          >
            <Input
              type="text"
              data-testid="inputemail"
              prefix={
                <MailOutlined className="site-form-item-icon" />
              }
              placeholder={t('email')}
            />
          </Form.Item>

          <Form.Item>
            <Button
              block
              type="primary"
              htmlType="submit"
              loading={isLoading}
              className="login-form-button"
              data-testid="forgotPassword_button"
            >
              {t('tasks.reset_password')}
            </Button>
            <Divider />
          </Form.Item>
        </Form>
      ) : (
        <Form.Item
          className="form--container"
          style={{ textAlign: 'center' }}
        >
          <MailOutlined
            style={{
              fontSize: 100,
              color: '#3897eb',
              opacity: 0.8,
              marginBottom: '1rem',
            }}
          />
          <Alert message={t('success.forgot_password')} />
        </Form.Item>
      )}
    </>
  );
}

ForgotPassword.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(ForgotPassword);
