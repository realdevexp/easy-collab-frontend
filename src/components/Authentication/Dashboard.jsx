import React from 'react';
import { withTranslation } from 'react-i18next';
import { func } from 'prop-types';

const Dashboard = ({ t }) => (
  <div>
    <h1>{t('success.welcome_to_easy_collab')}</h1>
  </div>
);

Dashboard.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(Dashboard);
