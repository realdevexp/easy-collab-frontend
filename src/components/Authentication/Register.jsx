/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Checkbox, Divider } from 'antd';
import {
  UserOutlined,
  LockOutlined,
  MailOutlined,
} from '@ant-design/icons';
import {
  Link,
  Redirect,
  BrowserRouter as Router,
} from 'react-router-dom';
import { useMutation } from 'react-query';
import { withTranslation } from 'react-i18next';

import { func } from 'prop-types';
import { registerUser } from '../../api/auth';

const Register = ({ t }) => {
  const [form] = Form.useForm();
  const [isMailInUse, setIsMailInUse] = useState(false);
  const { mutate, isSuccess, isError, isLoading, error } =
    useMutation(registerUser);

  useEffect(() => {
    if (isError && error.response.status === 409) {
      setIsMailInUse(true);
      form.validateFields();
    }
  }, [isError, error, form]);

  const onFinish = (inputs) => {
    // console.log('Received values of form: ', inputs);
    mutate(inputs);
  };

  if (isSuccess) {
    return <Redirect to="/Dashboard" />;
  }

  return (
    <Form
      form={form}
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="fullname"
        data-testid="fullname"
        rules={[
          {
            required: true,
            message: t('form_errors.enter_full_name'),
          },
          {
            min: 4,
            message: t('form_errors.name_atleast_four_chars'),
          },
          {
            max: 20,
            message: t('form_errors.name_max_twenty_chars'),
          },
          {
            pattern: new RegExp(
              /^[A-Za-z\s]{1,}[.]{0,1}[A-Za-z\s]{0,}$/,
            ),
            message: t('form_errors.valid_name'),
          },
        ]}
      >
        <Input
          type="text"
          data-testid="inputFullName"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder={t('name')}
        />
      </Form.Item>
      <Form.Item
        name="email"
        data-testid="email"
        rules={[
          {
            required: true,
            message: t('form_errors.email_required'),
          },
          {
            pattern: new RegExp(
              /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
            ),
            message: t('form_errors.valid_email_id'),
          },
          {
            validator: () =>
              !isMailInUse
                ? Promise.resolve()
                : Promise.reject(new Error(t('errors.email_in_use'))),
          },
        ]}
      >
        <Input
          type="email"
          data-testid="inputEmail"
          onChange={() =>
            isMailInUse ? setIsMailInUse(false) : null
          }
          prefix={<MailOutlined className="site-form-item-icon" />}
          placeholder={t('email')}
        />
      </Form.Item>
      <Form.Item
        name="password"
        data-testid="password"
        rules={[
          {
            required: true,
            message: t('form_errors.password_not_found'),
          },
          {
            min: 8,
            message: t('form_errors.password_min_eight_chars'),
          },
        ]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          data-testid="inputPassword"
          type="password"
          autoComplete="new-password"
          placeholder={t('password')}
        />
      </Form.Item>
      <Form.Item
        name="confirmpassword"
        data-testid="confirmPassword"
        dependencies={['password']}
        rules={[
          {
            required: true,
            message: t('form_errors.password_confirmation'),
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                new Error(t('errors.passwords_do_not_match')),
              );
            },
          }),
        ]}
        hasFeedback
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          data-testid="inputConfirmPassword"
          type="password"
          autoComplete="new-password"
          placeholder={t('tasks.confirm_password')}
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox data-testid="remember-checkbox">
            {t('tasks.remember_me')}
          </Checkbox>
        </Form.Item>
        <Router>
          <Link
            className="login-form-forgot"
            data-testid="remember-link"
            style={{ float: 'right' }}
            to="/forgot_password"
          >
            {t('forgot_password')}
          </Link>
        </Router>
      </Form.Item>
      <Form.Item>
        <Button
          type="primary"
          loading={isLoading}
          htmlType="submit"
          className="login-form-button"
          data-testid="register_button"
        >
          {t('tasks.register')}
        </Button>
        {'\n'}
        Or
        {'\n'}
        <Button
          type="ghost"
          htmlType="submit"
          className="login-form-button"
          data-testid="github_register_button"
          onClick={() =>
            window.open(
              'http://api.realdev.in/oauth/redirect/github',
              '_self',
            )
          }
        >
          {t('tasks.login_with_github')}
        </Button>
        <Divider plain>OR</Divider>
        <Router>
          {' '}
          <Link to="/login" data-testid="login_now_link">
            {' '}
            {t('tasks.login_now')}
          </Link>
        </Router>
      </Form.Item>
    </Form>
  );
};

Register.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(Register);
