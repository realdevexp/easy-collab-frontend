/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { Form, Input, Checkbox, Divider } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { withTranslation } from 'react-i18next';
import { useMutation } from 'react-query';
import { func } from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import { loginUser } from '../../api/auth';
import Button from '../Button/index';

function Login({ t }) {
  const [form] = Form.useForm();
  const [isUserRegistered, setisUserRegistered] = useState(false);
  const { mutate, isError, error, isSuccess, isLoading } =
    useMutation(loginUser);

  useEffect(() => {
    if (isError && error.response.status === 401) {
      setisUserRegistered(true);
      form.validateFields();
    }
  }, [isError, error, form]);

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    mutate(values);
  };
  if (isSuccess) {
    return <Redirect to="/Dashboard" />;
  }
  return (
    <Form
      form={form}
      name="normal_login"
      className="login-form"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Form.Item
        name="email"
        rules={[
          {
            required: true,
            message: t('form_errors.enter_your_email'),
          },
          {
            validator: () =>
              !isUserRegistered
                ? Promise.resolve()
                : Promise.reject(
                    new Error(t('errors.user_not_found_register')),
                  ),
          },
        ]}
      >
        <Input
          type="text"
          data-testid="email"
          prefix={<UserOutlined className="site-form-item-icon" />}
          placeholder={t('username_email')}
        />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[
          {
            required: true,
            message: t('form_errors.enter_your_password'),
          },
        ]}
      >
        <Input.Password
          prefix={<LockOutlined className="site-form-item-icon" />}
          type="password"
          placeholder={t('password')}
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>{t('tasks.remember_me')}</Checkbox>
        </Form.Item>

        <Link
          className="login-form-forgot"
          style={{ float: 'right' }}
          to="/forgot_password"
        >
          {t('forgot_password')}
        </Link>
      </Form.Item>
      <Form.Item>
        {/* tailwind css demo button */}
        <Button
          variant="large"
          type="submit"
          color="blue"
          loadingText={t('loading')}
          loading={isLoading}
        >
          {t('tasks.login')}
        </Button>
        <Divider />
        <p>
          {t('status.no_account_yet')}
          <Link to="/register"> {t('tasks.create_an_account')}</Link>
        </p>
        <Divider plain>OR</Divider>
      </Form.Item>
    </Form>
  );
}

Login.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(Login);
