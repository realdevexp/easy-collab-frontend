/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Divider, Result } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { withTranslation } from 'react-i18next';
import { useMutation } from 'react-query';
import { func } from 'prop-types';
import { useLocation } from 'react-router-dom';
import { resetPassword } from '../../api/auth';

// Hook to grab the query String from the URL
function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const ResetPassword = ({ t }) => {
  const [form] = Form.useForm();
  const [isUserRegistered, setisUserRegistered] = useState(false);
  const token = useQuery().get('token');

  const { mutate, isError, error, isSuccess, isLoading } =
    useMutation(resetPassword);

  useEffect(() => {
    if (isError && error.response.status === 401) {
      setisUserRegistered(true);
      form.validateFields();
    }
  }, [isError, error, form]);

  useEffect(() => {
    console.log(isUserRegistered);
  }, [isUserRegistered]);

  const onFinish = (values) => {
    const { newPassword, verifyPassword } = values;
    mutate({ token, newPassword, verifyPassword });
    console.log('onFinish Ran!');
  };

  return (
    <>
      {!isSuccess ? (
        <Form
          form={form}
          name="normal_login"
          className="form--container login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
        >
          <Form.Item
            name="newPassword"
            data-testid="newpassword"
            rules={[
              {
                required: true,
                message: t('form_errors.password_not_found'),
              },
              {
                min: 8,
                message: t('form_errors.password_min_eight_chars'),
              },
            ]}
            hasFeedback
          >
            <Input.Password
              prefix={
                <LockOutlined className="site-form-item-icon" />
              }
              data-testid="inputnewpassword"
              type="password"
              placeholder={t('new_password')}
            />
          </Form.Item>
          <Form.Item
            name="verifyPassword"
            data-testid="verifypassword"
            dependencies={['newPassword']}
            rules={[
              {
                required: true,
                message: t('form_errors.password_confirmation'),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (
                    !value ||
                    getFieldValue('newPassword') === value
                  ) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(t('errors.passwords_do_not_match')),
                  );
                },
              }),
            ]}
            hasFeedback
          >
            <Input.Password
              prefix={
                <LockOutlined className="site-form-item-icon" />
              }
              type="password"
              autoComplete="new-password"
              data-testid="inputverifypassword"
              placeholder={t('tasks.confirm_password')}
            />
          </Form.Item>

          <Form.Item>
            <Button
              block
              type="primary"
              htmlType="submit"
              loading={isLoading}
              className="login-form-button"
              data-testid="resetpasswordbutton"
            >
              {t('tasks.reset_password')}
            </Button>
            <Divider />
          </Form.Item>
        </Form>
      ) : (
        <Form.Item
          className="form--container"
          style={{ textAlign: 'center' }}
        >
          <Result
            status="success"
            subTitle="Password Reset Successfully!"
          />
        </Form.Item>
      )}
    </>
  );
};

ResetPassword.propTypes = {
  t: func.isRequired,
};

export default withTranslation()(ResetPassword);
