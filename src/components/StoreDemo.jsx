import React from 'react';
import userStore from '../store/user';

function StoreDemo() {
  const updateUsername = userStore(({ setUsername }) => setUsername);
  const userName = userStore(({ username }) => username);

  const updateUname = () => {
    updateUsername('Some new username');
  };

  return (
    <div>
      <h1>{userName}</h1>
      <button onClick={updateUname} type="button">
        Update User name
      </button>
    </div>
  );
}

export default StoreDemo;
