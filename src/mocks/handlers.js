/* eslint-disable import/no-extraneous-dependencies */
import { rest } from 'msw';
import { host } from '../api/auth';
import {
  loginValidateSchema,
  registerValidateSchema,
  forgotPasswordValidateSchema,
  resetPasswordValidateSchema,
} from './validation';

const handlers = [
  rest.get(`${host}/`, (req, res, ctx) =>
    res(ctx.body('<h1>Hello world</h1>')),
  ),
  rest.post(`${host}/register`, (req, res, ctx) => {
    const { email, fullname, password, confirmpassword } = req.body;
    const validate = registerValidateSchema.validate({
      email,
      fullname,
      password,
      confirmpassword,
    });
    if (validate.error) {
      console.log(validate.error);
      return res(
        ctx.status(509),
        ctx.json({
          msg: 'string',
          token: 'string',
        }),
      );
    }
    return res(ctx.json({ ok: true }));
  }),

  rest.post(`${host}/login`, (req, res, ctx) => {
    const { email, password } = req.body;
    const validate = loginValidateSchema.validate({
      email,
      password,
    });
    if (validate.error) {
      console.log(validate.error);
      return res(
        ctx.status(509),
        ctx.json({ Error: 'Some validation error' }),
      );
    }
    return res(
      ctx.json({
        msg: 'string',
        token: 'string',
      }),
    );
  }),

  rest.post(`${host}/forgot_password`, (req, res, ctx) => {
    const { email } = req.body;
    const validate = forgotPasswordValidateSchema.validate({ email });
    if (validate.error) {
      // console.log(validate.error);
      return res(
        ctx.status(509),
        ctx.json({ Error: 'Some validation error' }),
      );
    }
    return res(
      ctx.json({
        msg: 'string',
      }),
    );
  }),
  rest.post(`${host}/rest_password`, (req, res, ctx) => {
    const { newPassword, verifyPassword } = req.body;
    const validate = resetPasswordValidateSchema.validate({
      newPassword,
      verifyPassword,
    });
    if (validate.error) {
      return res(
        ctx.status(509),
        ctx.json({ Error: 'Some vqlidation error' }),
      );
    }
    return res(
      ctx.json({
        msg: 'string',
      }),
    );
  }),
];
export default handlers;
