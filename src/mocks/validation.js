/* eslint-disable import/no-extraneous-dependencies */
import Joi from 'joi';

export const registerValidateSchema = Joi.object({
  fullname: Joi.string().required(),
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required(),
  password: Joi.string().min(8).required(),
  confirmpassword: Joi.valid(Joi.ref('password')),
});

export const loginValidateSchema = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required(),
  password: Joi.string().min(8).required(),
});

export const forgotPasswordValidateSchema = Joi.object({
  email: Joi.string()
    .email({ tlds: { allow: false } })
    .required(),
});

export const resetPasswordValidateSchema = Joi.object({
  newPassword: Joi.string().min(8).required(),
  verifyPassword: Joi.valid(Joi.ref('newPassword')),
});

export default undefined;
