import create from 'zustand';

const userStore = create((set) => ({
  username: 'Default user name',
  setUsername: (username) => set(() => ({ username })),
}));

export default userStore;
