import React from 'react';
import ReactDOM from 'react-dom';
import './internationalization/index';
import App from './components/App';
import 'antd/dist/antd.css';
// uncomment to enable mock service worker
// const startMockServiceWorker = async () => {
//   const { worker } = await import('./mocks/browser');
//   worker.start({
//     waitUntilReady: true,
//     onUnhandledRequest: (req) => {
//       console.error(
//         'Found an unhandled %s request to %s',
//         req.method,
//         req.url.href,
//       );
//     },
//   });
// };
// if (import.meta.env.MODE === 'development') {
//   startMockServiceWorker();
// }

ReactDOM.render(<App />, document.getElementById('app'));

if (import.meta.hot) {
  import.meta.hot.accept();
}
