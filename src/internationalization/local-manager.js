// en
import commonEN from './locales/en.json';
import commonHN from './locales/hn.json';

const resources = {
  en: {
    common: commonEN,
  },
  hn: {
    common: commonHN,
  },
};

export default resources;
