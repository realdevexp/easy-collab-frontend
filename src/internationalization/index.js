import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import resources from './local-manager';

const languages = {
  EN: 'en',
  HN: 'hn',
};

export default i18n.use(initReactI18next).init({
  resources,
  fallbackLng: languages.EN,
  keySeparator: '.',
  ns: ['common'],
  defaultNS: 'common',
  whitelist: [languages.EN, languages.HN],
  interpolation: { escapeValue: false },
});
