// eslint-disable no-console
import ky from 'ky';

export const host = import.meta.env.SNOWPACK_PUBLIC_BACKEND_HOST;

const api = ky.create({
  prefixUrl: host,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export const signInWithGithub = async (code) => {
  const { token } = await api
    .post('oauth/signin/github', {
      json: { code },
    })
    .json();
  return token;
};

export const registerUser = async (inputs) => {
  const response = await api
    .post('auth/register', { json: inputs })
    .json();
  return response;
};

export const loginUser = async (inputs) => {
  const response = await api
    .post('auth/login', { json: inputs })
    .json();
  console.log(response);
  return response;
};

export const forgotPassword = async (inputs) => {
  const response = await api
    .post('auth/forgot_password', { json: inputs })
    .json();
  return response;
};

export const resetPassword = async (inputs) => {
  const response = await api
    .post('auth/reset_password', { json: inputs })
    .json();
  console.log('message', response);
  return response;
};
