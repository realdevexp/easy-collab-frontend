FROM node:14-alpine as builder

LABEL version="1.0"
LABEL description="This is the base docker image for the Project Management Tool frontend in React."
LABEL maintainer = ["info@realdev.in"]

# copy the package.json to install dependencies
COPY ["package.json", "yarn.lock", "./"]

# Install the dependencies and make the folder
RUN yarn && mkdir /app && mv ./node_modules ./app

WORKDIR /app

COPY . .

# Build the project and copy the files
RUN yarn build

FROM nginx:alpine

# COPY nginx config file /
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf

## Remove default nginx index page
RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /app/build /usr/share/nginx/html

EXPOSE 3000 80

CMD ["nginx", "-g", "daemon off;"]