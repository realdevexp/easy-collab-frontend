module.exports = {
  // mode: 'jit',
  purge: ['./public/**/*.html', './src/**/*.{js,jsx,svg}'],
  variants: {
    extend: {
      opacity: ['disabled'],
    },
  },
};
