import { screen, fireEvent, waitFor } from '@testing-library/react';
import { expect } from 'chai';

export const testIds = (testcases, renderCb) => {
  testcases.forEach((testcase) => {
    it(testcase.des, () => {
      renderCb();
      const El = screen.getByTestId(testcase.testid);
      expect(document.body.contains(El));
    });
  });
};

export const inputRequiredValidationIds = (testcases, renderCb) => {
  testcases.forEach((testcase) => {
    it(testcase.des, async () => {
      renderCb();
      const submitButton = screen.getByTestId(testcase.buttonId);
      fireEvent.click(submitButton);
      const validateError = await waitFor(() =>
        screen.getByText(testcase.validationText),
      );
      expect(document.body.contains(validateError));
    });
  });
};

export const userInputValidationIds = (testcases, renderCb) => {
  testcases.forEach((testcase) => {
    it(testcase.des, async () => {
      renderCb();
      const submitButton = screen.getByTestId(testcase.buttonId);
      const userInput = screen.getByTestId(testcase.inputID);
      fireEvent.change(userInput, {
        target: { value: testcase.inputText },
      });
      fireEvent.click(submitButton);
      const validateError = await waitFor(() =>
        screen.getByText(testcase.validationText),
      );
      expect(document.body.contains(validateError));
    });
  });
};

export const forgotPasswordSuccess = (testcase, renderCb) => {
  it(testcase.des, async () => {
    renderCb();
    const submitButton = screen.getByTestId(testcase.buttonId);
    const userInput = screen.getByTestId(testcase.inputID);
    fireEvent.change(userInput, {
      target: { value: testcase.inputText },
    });

    fireEvent.click(submitButton);
    const successScreen = await waitFor(() =>
      screen.getByText(testcase.validationText),
    );
    expect(document.body.contains(successScreen));
  });
};

export const resetPasswordSuccess = (testcase, renderCb) => {
  it(testcase.des, async () => {
    renderCb();
    const submitButton = screen.getByTestId(testcase.buttonId);
    const userInput = screen.getByTestId(testcase.inputID);
    fireEvent.change(userInput, {
      target: { value: testcase.inputText },
    });

    fireEvent.click(submitButton);
    const successScreen = await waitFor(() =>
      screen.getByText(testcase.validationText),
    );
    expect(document.body.contains(successScreen));
  });
};
