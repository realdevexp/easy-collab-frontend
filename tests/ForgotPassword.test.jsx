import React, { useEffect } from 'react';
import { render } from '@testing-library/react';

import { QueryClientProvider, QueryClient } from 'react-query';
import { useTranslation } from 'react-i18next';
import { string } from 'prop-types';

import { worker } from '../src/mocks/browser';
import 'antd/dist/antd.css';

import '../src/internationalization/index';
import ForgotPassword from '../src/components/Authentication/ForgotPassword';

import {
  testIds,
  inputRequiredValidationIds,
  userInputValidationIds,
  forgotPasswordSuccess,
} from './testComponents';
import getForgotPasswordIDs from './TestIDs/ForgotPasswordIDs';
import resources from '../src/internationalization/local-manager';

const ForgotPasswordTest = ({ lang }) => {
  const i18n = useTranslation()[1];
  const queryClient = new QueryClient();
  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [i18n, lang]);
  return (
    <QueryClientProvider client={queryClient}>
      <ForgotPassword />
    </QueryClientProvider>
  );
};
ForgotPasswordTest.propTypes = {
  lang: string.isRequired,
};

before(() => {
  worker.start({ quiet: true });
});

after(() => worker.stop());

describe('ForgotPassword', () => {
  const langTest = (currentLang, local) => {
    const {
      ValidateUserInput,
      cases,
      requiredInputValidationCases,
      successPage,
    } = getForgotPasswordIDs(local);

    testIds(cases, () =>
      render(<ForgotPasswordTest lang={currentLang} />),
    );
    inputRequiredValidationIds(requiredInputValidationCases, () =>
      render(<ForgotPasswordTest lang={currentLang} />),
    );
    userInputValidationIds(ValidateUserInput, () =>
      render(<ForgotPasswordTest lang={currentLang} />),
    );
    forgotPasswordSuccess(successPage, () =>
      render(<ForgotPasswordTest lang={currentLang} />),
    );
  };
  const langs = Object.keys(resources);
  langs.forEach((lang) => {
    langTest(lang, resources[lang].common);
  });
});
