import React, { useEffect } from 'react';
import { render } from '@testing-library/react';
import { QueryClientProvider, QueryClient } from 'react-query';
import { useTranslation } from 'react-i18next';
import { string } from 'prop-types';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';

import { worker } from '../src/mocks/browser';
import 'antd/dist/antd.css';

import '../src/internationalization/index';
import ResetPassword from '../src/components/Authentication/ResetPassword';

import {
  testIds,
  inputRequiredValidationIds,
  userInputValidationIds,
  resetPasswordSuccess,
} from './testComponents';
import getResetPasswordIDs from './TestIDs/ResetPasswordIDs';
import resources from '../src/internationalization/local-manager';

const ResetPasswordTest = ({ lang }) => {
  const history = createMemoryHistory();
  const i18n = useTranslation()[1];
  const queryClient = new QueryClient();
  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [i18n, lang]);
  return (
    <Router history={history}>
      <QueryClientProvider client={queryClient}>
        <ResetPassword />
      </QueryClientProvider>
    </Router>
  );
};
ResetPasswordTest.propTypes = {
  lang: string.isRequired,
};

before(() => {
  worker.start({ quiet: true });
});

after(() => worker.stop());

describe('ResetPassword', () => {
  const langTest = (currentLang, local) => {
    const {
      ValidateUserInput,
      cases,
      requiredInputValidationCases,
      successPage,
    } = getResetPasswordIDs(local);

    testIds(cases, () =>
      render(<ResetPasswordTest lang={currentLang} />),
    );
    inputRequiredValidationIds(requiredInputValidationCases, () =>
      render(<ResetPasswordTest lang={currentLang} />),
    );
    userInputValidationIds(ValidateUserInput, () =>
      render(<ResetPasswordTest lang={currentLang} />),
    );

    resetPasswordSuccess(successPage, () =>
      render(<ResetPasswordTest lang={currentLang} />),
    );
  };
  const langs = Object.keys(resources);
  langs.forEach((lang) => {
    langTest(lang, resources[lang].common);
  });
});
