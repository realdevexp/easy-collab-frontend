module.exports = {
  presets: ['@babel/presets-react'],
  transform: {
    '^.+\\.jsx?$': './path/to/wrapper.js',
  },
};
