// NODE_ENV=test - Needed by "@snowpack/web-test-runner-plugin"
process.env.NODE_ENV = 'test';
const str = require('@snowpack/web-test-runner-plugin')();
module.exports = {
  testFramework: {
    config: {
      ui: 'bdd',
      timeout: '4000',
    },
  },
  plugins: [str,]
};